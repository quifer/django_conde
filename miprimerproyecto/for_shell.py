from preguntasyrespuestas.models import Pregunta, Respuesta
from django.utils import timezone

p = Pregunta(asunto="Quien es tu escritor favorito", descripcion="", fecha_publicacion=timezone.now())
p.save()

p1 = Pregunta(asunto="Cual es tu mes favorito", descripcion="", fecha_publicacion=timezone.now())
p1.save()

p.respuesta_set.create(contenido="Mi escritor fav. es Halim")
p1.respuesta_set.create(contenido="Es marzo")